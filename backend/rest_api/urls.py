from django.urls import path, include
from .views import *
from rest_framework.routers import DefaultRouter
from django.apps import apps
from django.conf import settings


router = DefaultRouter()
for project_app in settings.PROJECT_APPS:
    for model_name in apps.all_models[project_app.split('.')[0]]:
        router.register(r'(?P<model>.+)', ModelsViewSet, basename=model_name)

urlpatterns = [
    path('', include(router.urls)),
]
