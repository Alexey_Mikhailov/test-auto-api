from rest_framework import serializers


class SimpleSerializer(serializers.Serializer):
    label = serializers.CharField(max_length=100)


class ItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = None
        fields = '__all__'
