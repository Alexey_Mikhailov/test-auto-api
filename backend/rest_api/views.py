from .serializers import ItemSerializer, SimpleSerializer
from rest_framework import viewsets, filters
from django_filters.rest_framework import DjangoFilterBackend
from django.apps import apps
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.conf import settings


class ModelsViewSet(viewsets.ModelViewSet):
    filter_backends = [filters.OrderingFilter, DjangoFilterBackend]
    filterset_fields = '__all__'
    ordering_fields = '__all__'

    def get_model(self):
        model_name = self.kwargs.get('model')
        return apps.get_model(model_name)

    def get_queryset(self):
        return self.get_model().objects.all()

    def get_serializer_class(self):
        serializer = ItemSerializer
        serializer.Meta.model = self.get_model()
        return serializer


@api_view(['GET'])
def api_root(request):
    project_apps = [app.split('.')[0] for app in settings.PROJECT_APPS]
    names = [i._meta for i in apps.get_models() if i._meta.app_label in project_apps]
    serializer = SimpleSerializer(names, many=True)
    return Response(serializer.data)
