from django.db import models


class ToDoItem(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=200)
    text = models.TextField(verbose_name='Описание')
    created = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)

    def __str__(self):
        return f'Задача "{self.title}" от {self.created.isoformat()}'

    class Meta:
        ordering = ['-created']
